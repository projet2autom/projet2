package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class OrderPage extends AbstractPage {
    public OrderPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//section[@id='checkout-personal-information-step']//h1[@class='step-title js-step-title h3']")
    private WebElement pageInfoTitle;

    @FindBy(xpath = "//article[@id='id_address_delivery-address-2']//span[@class='custom-radio']//span")
    private WebElement frenchRadio;

//    @FindBy(xpath = "//input[@id='field-address1']")
//    private WebElement addressField;
//
//    @FindBy(xpath = "//input[@id='field-postcode']")
//    private WebElement postcodeField;
//
//    @FindBy(xpath = "//input[@id='field-city']")
//    private WebElement cityField;

    @FindBy(xpath = "//button[@name='confirm-addresses']")
    private WebElement continuerButton1;

    @FindBy(xpath = "//section[@id='checkout-delivery-step']//h1[@class='step-title js-step-title h3']")
    private WebElement pageDeleveryTitle;

    @FindBy(xpath = "//div[@class='delivery-options']//div[1]//div[1]//span[1]//span[1]")
    private WebElement shopCollect;

    @FindBy(xpath = "//button[@name='confirmDeliveryOption']")
    private WebElement continuerButton2;

    @FindBy(xpath = "//section[@id='checkout-payment-step']//h1[@class='step-title js-step-title h3']")
    private WebElement pagePaymentTitle;

    @FindBy(xpath = "//input[@id='payment-option-2']")
    private WebElement payByCard;

    @FindBy(xpath = "//input[contains(@id,'conditions_to_approve')]")
    private WebElement conditionsGenerales;

    @FindBy(xpath = "//button[normalize-space()='Commander']")
    private WebElement commanderButton;

    @FindBy(xpath = "//h3[@class='h1 card-title']")
    private WebElement pageConfirmTitle;

    @FindBy(xpath = "//div[@id='order-items']/div[2]/div[1]/div[3]/div[1]/div")
    private List<WebElement> dimension40;

    @FindBy(xpath = "//div[@id='order-items']/div[2]/div[2]/div[3]/div[1]/div")
    private List<WebElement> dimension60;

    @FindBy(xpath = "//tr[@class='total-value font-weight-bold']//td[2]")
    private WebElement totalTTC;

    // Méthode de clic sur adresse francaise
    public void clickFrenchRadio (WebDriverWait wait) throws InterruptedException {
        if (!frenchRadio.isSelected()) {
            seleniumTools.clickOnElement(wait, frenchRadio);
        }
    }
//    public void selectAddress (WebDriverWait wait, String address, String postcode, String city) throws InterruptedException {
//        if (addressField.isDisplayed()) {
//            seleniumTools.sendKey(wait, addressField, address);
//            seleniumTools.sendKey(wait, postcodeField, postcode);
//            seleniumTools.sendKey(wait, cityField, city);
//        }
//        else {
//            if (!frenchRadio.isSelected()) {
//                seleniumTools.clickOnElement(wait, frenchRadio);
//            }
//        }
//    }

    // Méthode de clic sur Continuer/ Confirmer l'adresse
    public void clickContinuer1 (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, continuerButton1);
    }

    // Méthode de clic sur Retrait en magasin
    public void clickShopCollect (WebDriverWait wait) throws InterruptedException {
        if (!shopCollect.isSelected()) {
            seleniumTools.clickOnElement(wait, shopCollect);
        }
    }

    // Méthode de clic sur Continuer/ Confirmer mode de livraison
    public void clickContinuer2 (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, continuerButton2);
    }

    // Méthode de clic sur Payment par virement bancaire
    public void clickPayByCard (WebDriverWait wait) throws InterruptedException {
        if (!payByCard.isSelected()) {
            //seleniumTools.clickOnElement(wait, payByCard);
            payByCard.click();
        }
    }

    // Méthode de clic sur Conditions générales
    public void clickConditionsGenerales (WebDriverWait wait) throws InterruptedException {
        if (!conditionsGenerales.isSelected()) {
            //seleniumTools.clickOnElement(wait, conditionsGenerales);
            conditionsGenerales.click();
        }
    }

    // Méthode de clic sur Commander
    public RecapCommandPage clickCommander (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, commanderButton);
        return new RecapCommandPage(driver);
    }


    // Getters
    public String getPageInfoTitle(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(pageInfoTitle)).getText();
    }

    public String getPageDeliveryTitle(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(pageDeleveryTitle)).getText();
    }

    public String getPagePaymentTitle(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(pagePaymentTitle)).getText();
    }

    public String getPageConfirmTitle(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(pageConfirmTitle)).getText();
    }

    public List<String> getOrder40() {
        // Créer une liste pour stocker les textes des éléments
        List<String> order40 = new ArrayList<>();
        // Récupérer les textes des onglets
        for (WebElement element : dimension40) {
            String elementText = element.getText().trim();
            order40.add(elementText);
        }
        return order40;
    }

    public List<String> getOrder60() {
        // Créer une liste pour stocker les textes des éléments
        List<String> order60 = new ArrayList<>();
        // Récupérer les textes des onglets
        for (WebElement element : dimension60) {
            String elementText = element.getText().trim();
            order60.add(elementText);
        }
        return order60;
    }

    public Double getTotalTTC() {
        String totalTTCString = totalTTC.getText().substring(0, totalTTC.getText().length() - 1).replace(",", ".");
        return Double.parseDouble(totalTTCString);
    }
}
