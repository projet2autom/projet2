package org.prestashop.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WelcomePage extends AbstractPage {
    public WelcomePage(WebDriver driver) {
        super(driver);
    }

//    @FindBy(xpath = "//span[contains (text(),'Connexion')]")
//    private static WebElement connexion;

    @FindBy(xpath = "//h2[@class='h2 products-section-title text-uppercase']")
    private static WebElement produitsPopulaires;

    @FindBy(xpath = "//h3[@class='h3 product-title']/a")
    private List<WebElement> productsList;

//    @FindBy(xpath = "//span[@class='cart-products-count']")
//    private static WebElement panier;

    @FindBy(xpath="//title")
    public WebElement  titrePageAccueil;

    @FindBy (id="carousel")
    public WebElement   caroussel;

    @FindBy (xpath="//ul/li/a[@title=\"Identifiez-vous\"]")
    public WebElement  connexionFooter;

    @FindBy(xpath="//a[@title=\"Identifiez-vous\"]")
    private WebElement btnConnexion;

    //METHODES

//    // Méthode de clic sur "Connexion" pour accéder à la page de connexion
//    public ConnectionPage clickConnexion(WebDriverWait wait) throws InterruptedException {
//        seleniumTools.clickOnElement(wait, connexion);
//        return new ConnectionPage(driver);
//    }

    //click sur informations personnelles dans le footer -emmène sur la page de connexion
    public ConnectionPage clickConnexionFooter(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,connexionFooter);
        return new ConnectionPage(driver);
    }

    //Getters
//    public static WebElement getConnexion() {
//        return connexion;
//    }

    public static WebElement getProduitsPopulaires() {
        return produitsPopulaires;
    }

    public List<WebElement> getProductsList() {
        return productsList;
    }

//    public static WebElement getPanier() {
//        return panier;
//    }

    public ProductPage clickOnProduct(String product) {
        String path = String.format("//a[contains(text(),'%s')]",product);
        driver.findElement(By.xpath(path)).click();
        return new ProductPage(driver);
    }

    public ConnectionPage clickConnexion(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,btnConnexion);
        return new ConnectionPage(driver);
    }
}
