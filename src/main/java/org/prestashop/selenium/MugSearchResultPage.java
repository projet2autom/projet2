package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class MugSearchResultPage extends AbstractPage{
    public MugSearchResultPage(WebDriver driver) {super(driver);}

    //Locators
    @FindBy(xpath="//h2[@class='h3 product-title']")
    protected List<WebElement> mugsList;

    @FindBy(xpath="//a[normalize-space()='Mug Today is a good day']")
    protected WebElement mugSearched;

    //Méthode de clic sur le produit cherché pour accéder à la page du produit
    public MugSearchedPage clickMugSearched (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,mugSearched);
        return new MugSearchedPage(driver);
    }

    //Getters
    public List<String> getMugsList() {
        List<String> mugslist = new ArrayList<>();
        for (WebElement mug : mugsList) {
            String elementText = mug.getText().trim();
            mugslist.add(elementText);
    }
        return mugslist;
    }
}
