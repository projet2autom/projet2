package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPage extends AbstractPage {
    public AccountPage(WebDriver driver) {
        super(driver);
    }

    //LOCATORS
//    @FindBy(xpath = "//a/span[@class='hidden-sm-down']")
//    private WebElement connectedUser;
//
//    @FindBy(xpath = "//a[@class='logout hidden-sm-down']")
//    private WebElement deconnexion;
//
//    @FindBy(xpath = "//a[normalize-space()='Art']")
//    private WebElement artCategory;

    @FindBy(id="field-id_gender-1")
    private WebElement checkbox_M;

    @FindBy(id="field-firstname")
    private WebElement  firstnameField;

    @FindBy(id="field-lastname")
    private WebElement lastnameField;

    @FindBy(id="field-email")
    private WebElement emailField;

    @FindBy(id="field-birthday")
    private WebElement birthdayField;

    @FindBy(id="field-password")
    private WebElement oldPasswordField;

    @FindBy(id="field-new_password")
    private WebElement newPasswordField;

    @FindBy (xpath="//input[@name=\"optin\"]")
    private WebElement optinChexbox;

     @FindBy(xpath="//input[@name=\"newsletter\"]")
     private WebElement newsletterCheckbox;

     @FindBy(xpath="//input[@name=\"psgdpr\"]")
     private WebElement conditionGenerales;

     @FindBy(xpath="//input[@name=\"customer_privacy\"]")
     private WebElement  customerPrivacy;

     @FindBy(xpath="//input[@id=\"field-password\"]/../span/button")
     private WebElement showOldPwd;

     @FindBy(xpath="//input[@id=\"field-new_password\"]/../span/button")
     private WebElement showNewPwd;

     @FindBy(xpath = "//button[@class=\"btn btn-primary form-control-submit float-xs-right\"]")
     private WebElement boutonEnregistrer;

    @FindBy(xpath="//article[@class=\"alert alert-success\"]")
    private WebElement alerteSucces;

    //METHODS


     //Méthode de clic sur Art pour accéder à la page Art
    @FindBy(xpath="//a[img]")
    protected WebElement homePageButton;

    // Méthode de clic sur Art pour accéder à la page Art
//    public ArtPage clickArt(WebDriverWait wait) throws InterruptedException {
//        seleniumTools.clickOnElement(wait, artCategory);
//        return new ArtPage(driver);
//    }

    public void fillOldPassword(WebDriverWait wait,String pwd) throws InterruptedException {
        seleniumTools.sendKey(wait, oldPasswordField, pwd);
    }

    public void fillNewPassword(WebDriverWait wait, String newPwd) throws InterruptedException {
        seleniumTools.sendKey(wait, newPasswordField, newPwd);
    }

    public void checkChekbox(WebElement el){
        boolean isSelected = el.isSelected(); // vérifier si la case est cochée
        if(!isSelected){ // si non, la cocher
            el.click();
        }
    }

    public void uncheckChekbox(WebElement el){
        boolean isSelected = el.isSelected(); // vérifier si la case est cochée
        if(isSelected = true){ // si oui la déselectionner
            el.click();
        }
    }

    public void clickShowOldPassword(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, showOldPwd);
    }

    public void clickShowNewPassword(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, showNewPwd);
    }

    public void clickButtonEnregistrer(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, boutonEnregistrer);
    }
    // Getters
//    public String getConnectedUser(WebDriverWait wait){
//        return wait.until(ExpectedConditions.visibilityOf(connectedUser)).getText();
//    }
//
//    public String getDeconnexionText(WebDriverWait wait){
//        return wait.until(ExpectedConditions.visibilityOf(deconnexion)).getText();
//    }

    public void goToHomePage(){
        homePageButton.click();
    }



//GETTERS
    public WebElement getCheckbox_M(){return checkbox_M;}
    public WebElement getFirstnameField(){return firstnameField;}
    public WebElement getLastnameField() {return lastnameField;}
    public WebElement getEmailField() {return emailField;}
    public WebElement getBirthdayField() {return birthdayField;}
    public WebElement getOldPasswordField(){return oldPasswordField;}
    public WebElement getNewPasswordField(){return newPasswordField;}
    public WebElement getOptinChexbox(){return optinChexbox;}
    public WebElement getNewsletterCheckbox(){return newsletterCheckbox;}
    public WebElement getCustomerPrivacy(){return customerPrivacy;}
    public WebElement getConditionGenerales(){return conditionGenerales;}
    public WebElement getAlerteSucces(){return alerteSucces;}

}
