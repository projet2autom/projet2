package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountBoard extends AbstractPage {

   //CONSTRUCTEUR
    protected AccountBoard(WebDriver driver) {super(driver);}

    //LOCATORS
    @FindBy(id="identity-link")
    private WebElement information;

    //METHODES

    public AccountPage clickInformation(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,information);
        return new AccountPage(driver);
    }

    //GETTERS
    public WebElement getInformation(){return information;}

}
