package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConnectionPage extends AbstractPage {
    public ConnectionPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "field-email")
    private WebElement userField;

    @FindBy(id = "field-password")
    private WebElement passwordField;

    @FindBy(xpath = "//button[normalize-space()='Afficher']")
    private WebElement montrerButton;

    @FindBy(id = "submit-login")
    private WebElement submitButton;

    @FindBy(xpath="//a[contains(text(),\" Mot de passe oublié ?\")]")
    private WebElement  forgottenPwd;

    @FindBy(xpath = "//a[@data-link-action=\"display-register-form\"]")
    private WebElement createAccountButton;

    @FindBy(xpath = "//li[@class=\"alert alert-danger\"]")
    private WebElement authFail;

    // Méthode de remplir username
    public void fillUsername(WebDriverWait wait, String username) throws InterruptedException {
        seleniumTools.sendKey(wait, userField, username);
    }

    // Méthode de remplir password
    public void fillPassword(WebDriverWait wait, String password) throws InterruptedException {
        seleniumTools.sendKey(wait, passwordField, password);
    }

    // Méthode de clic sur Connexion pour accéder à la page Votre compte
    public AccountPage clickConnexion(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, submitButton);
        return new AccountPage(driver);
    }

    public AccountBoard clickConnexionFooter(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, submitButton );
        return  new AccountBoard(driver);
    }

    // GETTERS
    public  WebElement getUsernameField(){return userField;}

    public WebElement getPwdField(){return passwordField; }

    public WebElement getShowButton(){return montrerButton;}

    public WebElement getSubmitButton(){return submitButton;}

    public WebElement getForgottenPwd(){return forgottenPwd;}
    public WebElement getCreateAccountButton(){return createAccountButton;}
    public WebElement getAuthFail(){return authFail;}

}
