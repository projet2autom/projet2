package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BestProductPage extends AbstractPage {
    public BestProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h1[@class='h1']")
    private WebElement productName;

    @FindBy(xpath = "//span[@class='current-price-value']")
    private WebElement productPrice;

    @FindBy(id = "quantity_wanted")
    private WebElement quantityField;

    @FindBy(xpath = "//i[@class='material-icons touchspin-up']")
    private WebElement addQuantity;

    @FindBy(xpath = "//button[@class='btn btn-primary add-to-cart']")
    private WebElement ajouterAuPanierButton;

    @FindBy(id = "myModalLabel")
    private WebElement popupProductAdded;

    @FindBy(xpath = "//p[@class='cart-products-count']")
    private WebElement numberActicles;

    @FindBy(xpath = "//p[@class='product-price']")
    private WebElement unitPrice;

    @FindBy(xpath = "//span[@class='subtotal value']")
    private WebElement subTotal;

    @FindBy(xpath = "//span[@class='shipping value']")
    private WebElement transport;

    @FindBy(xpath = "//span[@class='value']")
    private WebElement totalTTC;

    @FindBy(xpath = "//button[contains (text(),'Continuer mes achats')]")
    private WebElement continueAchats;

//    @FindBy(xpath = "//span[@class='cart-products-count']")
//    private WebElement panier;

    @FindBy(id = "group_3")
    private WebElement dimensionDropdown;

    @FindBy(xpath = "//select[@id='group_3']/option[contains(text(),'60x90cm')]")
    private WebElement dimension60;

    @FindBy(xpath = "//select[@id='group_3']/option[contains(text(),'80x120cm')]")
    private WebElement dimension80;

    @FindBy(xpath = "//a[@class='btn btn-primary' and text()='Commander']")
    private WebElement commanderButton;

    //Méthode d'ajout 2 affiches 40x60
//    public void addProducts(WebDriverWait wait, String quantity) throws InterruptedException {
//        seleniumTools.sendKey(wait, quantityField, quantity);
//    }
    public void clickAddQuantity (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, addQuantity);
    }

    // Méthode de clic sur le bouton Ajouter Au Panier
    public void clickAjouterAuPanier (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, ajouterAuPanierButton);
        wait.until(ExpectedConditions.visibilityOf(unitPrice));
    }

    // Méthode de clic sur Continuer Achats
    public void clickContinuerAchats (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, continueAchats);
    }

    // Méthode de sélectionner la dimension 60x90cm
    public void selectDimension60 (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, dimensionDropdown);
        Thread.sleep(3000);
        seleniumTools.clickOnElement(wait, dimension60);
        Thread.sleep(3000); //??? wait ne marche pas
    }

    // Méthode de sélectionner la dimension 80x120cm
    public void selectDimension80 (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, dimensionDropdown);
        Thread.sleep(3000);
        seleniumTools.clickOnElement(wait, dimension80);
        Thread.sleep(3000); //??? wait ne marche pas
    }

    // Méthode de clic sur Commander pour accéder à la page Panier
    public CartPage clickCommander(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, commanderButton);
        return new CartPage(driver);
    }

    // Getters

    public String getProductName(WebDriverWait wait){
        return wait.until(ExpectedConditions.visibilityOf(productName)).getText();
    }

    public String getProductPrice(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(productPrice)).getText();
    }

    public WebElement getPopupProductAdded(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(popupProductAdded));
    }

    public String getNumberArticles(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(numberActicles)).getText();
    }

    public String getUnitPriceString() {
        return unitPrice.getText();
    }

    public Double getUnitPrice() {
        String unitPriceString = unitPrice.getText().substring(0, unitPrice.getText().length() - 1).replace(",", ".");
        return Double.parseDouble(unitPriceString);
    }

    public Double getSubTotal() {
        String subTotalString = subTotal.getText().substring(0, subTotal.getText().length() - 1).replace(",", ".");
        return Double.parseDouble(subTotalString);
    }

    public Double getTotalTTC() {
        String totalTTCString = totalTTC.getText().substring(0, totalTTC.getText().length() - 1).replace(",", ".");
        return Double.parseDouble(totalTTCString);
    }

    public String getTransport() {
        return transport.getText();
    }

//    public String getPanier(WebDriverWait wait) {
//        return wait.until(ExpectedConditions.visibilityOf(panier)).getText();
//    }
}
