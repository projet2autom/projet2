package org.prestashop.selenium;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HeadBand extends AbstractPage{

    //CONSTRUCTEUR
    public HeadBand(WebDriver driver) {super(driver);}
    //LOCATORS
    @FindBy (xpath="//a[@title=\"Voir mon compte client\"]/span")
    protected WebElement nomClient;

    @FindBy(xpath = "//a/span[@class='hidden-sm-down']")
    private WebElement connectedUser;

    @FindBy(xpath="//a[@class=\"logout hidden-sm-down\"]") //
    protected WebElement deconnexion;

    @FindBy(xpath="//a[@title=\"Identifiez-vous\"]/span")
    protected WebElement connexionTxt;

    @FindBy(xpath="//a[@title=\"Identifiez-vous\"]")
    protected WebElement btnConnexion;

    @FindBy(xpath="//a[parent::li[@id=\"category-9\"]]")
    protected WebElement artPage;

    @FindBy(xpath="//span[@class='cart-products-count']") ////div[@id="_desktop_cart"]
    protected WebElement panier;

    @FindBy(xpath="//input[@placeholder='Rechercher']")
    protected WebElement search;




    //METHODES
    // Méthode de clic sur le bouton Connexion
    public ConnectionPage clickbtnConnexion(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,btnConnexion);
        return new ConnectionPage(driver);
    }

    // Méthode de recherche un produit pour accéder à la page de résultat
    public MugSearchResultPage searchByKeyword(WebDriverWait wait, String keyword) throws InterruptedException {
        seleniumTools.sendKey(wait, search, keyword);
        search.sendKeys(Keys.ENTER);
        return new MugSearchResultPage(driver);
    }


    //GETTERS
    public String connexionIconText(){
        return connexionTxt.getText();
    }
    public WebElement getNomClient(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(nomClient));
        return nomClient;}
    public WebElement getDeconnexion(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(deconnexion));
        return deconnexion;}
    public WebElement getConnexionTxt(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(connexionTxt));
        return connexionTxt;}


    public WebElement getPanier(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(panier));
        return panier;
    }

    public ArtPage clickArt(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, artPage);
        return new ArtPage(driver);
    }

    public String getConnectedUser(WebDriverWait wait){
        return wait.until(ExpectedConditions.visibilityOf(connectedUser)).getText();
    }
}
