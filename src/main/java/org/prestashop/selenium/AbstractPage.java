package org.prestashop.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import org.prestashop.utils.SeleniumTools;


public class AbstractPage {
    protected static final Logger LOGGER = LogManager.getLogger();
    protected WebDriver driver;
    protected HeadBand headBand;

    protected final SeleniumTools seleniumTools = new SeleniumTools();

    /**
     * Constructeur extends sur tous les pages objets pour mutualisé la page factory
     * Possibilité de mettre le driver en static et éviter de le mettre dans le constructeur
     *
     * @param driver le webdriver utilisé dans le test
     */
    protected AbstractPage(WebDriver driver){
        this.driver = driver;
        SeleniumTools.myPageFactory(driver, AbstractPage.this);
        //headBand= new HeadBand(driver);
    }


    public HeadBand getHeadBand(){
        return headBand;
    }
}
