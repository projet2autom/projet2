package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PopUpRecapPage extends AbstractPage{

    protected PopUpRecapPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//p[@class=\"cart-products-count\"]")
    protected WebElement nbrArticle;

    @FindBy(xpath="//span[@class=\"shipping value\"]")
    protected WebElement coutTransport;

    @FindBy(xpath="//span[@class=\"value\"]")
    protected WebElement sousTotal;

    @FindBy(xpath="//a[parent::div[@class=\"cart-content-btn\"]]")
    protected WebElement commander;

    public String getNbrArticle(){ //todo
        String text = nbrArticle.getText();
        String prix = "34,46 €";

        return "";
    }

    public CartPage clickCommander(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,commander);
        return new CartPage(driver);
    }


}
