package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecapCommandPage extends AbstractPage{

    /**
     * Constructeur extends sur tous les pages objets pour mutualisé la page factory
     * Possibilité de mettre le driver en static et éviter de le mettre dans le constructeur
     *
     * @param driver le webdriver utilisé dans le test
     */
    protected RecapCommandPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//li[@id=\"order-reference-value\"]")
    protected WebElement reference;

    public String getReference(WebDriverWait wait){
        String ref = "pas de référence trouvé";
        String weTxt = wait.until(ExpectedConditions.visibilityOf(reference)).getText();
        System.out.println("text du we : '"+weTxt+"'");
        Pattern pattern = Pattern.compile("[A-Za-z0-9]+$");
        Matcher matcher = pattern.matcher(weTxt);
        if(matcher.find())
            ref = matcher.group();
        System.out.println("référence : "+ref);

        return ref;
    }
}
