package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MugSearchedPage extends AbstractPage{
    public MugSearchedPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//h1[@class='h1']")
    protected WebElement mugName;

    //Getters
    public WebElement getMugName() {
        return mugName;
    }
}
