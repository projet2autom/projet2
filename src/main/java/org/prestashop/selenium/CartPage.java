package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class CartPage extends AbstractPage{
    public CartPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//li[@class='cart-item']/div[1]/div[2]/div[3]/span[2]")
    private List<WebElement> productsDimensions;

    @FindBy(xpath = "//li[@class='cart-item']/div[1]/div[3]//div[1]//div[1]//div[1]//div[1]/input[1]")
    private List<WebElement> productQuantity;

    @FindBy(xpath = "//li[@class='cart-item']/div[1]/div[3]//div[1]//div[2]/span[1]/strong[1]")
    private List<WebElement> productsPrices;

    @FindBy(xpath = "//span[@class='label js-subtotal']")
    private WebElement productsQuantity;

    @FindBy(xpath = "//div[@id='cart-subtotal-products']/span[@class='value']")
    private WebElement subTotal;

    @FindBy(xpath = "//div[@class='cart-summary-line cart-total']/span[@class='value']")
    private WebElement totalTTC;

    @FindBy(xpath = "//li[3]//div[1]//div[3]//div[1]//div[3]//div[1]//a[1]//i[1]")
    private WebElement deleteProduct80;

    @FindBy(xpath = "//a[normalize-space()='Commander']")
    private WebElement commanderButton;

    // Méthode de supprimer l'affiche 80x120cm
    public void clickDeleteProduct80 (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, deleteProduct80);
        wait.until(ExpectedConditions.invisibilityOf(productsDimensions.get(2)));
    }

    // Méthode de clic sur Commander
    public OrderPage clickCommander (WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, commanderButton);
        return new OrderPage(driver);

    }

    //Getters
//    public List<WebElement> getProductDimensions() {
//        return productsDimensions;
//    }
    public List<String> getProductDimensions() {
        // Récupérer tous les éléments de la liste
//        List<WebElement> dimensions = productsDimensions;
        // Créer une liste pour stocker les textes des éléments
        List<String> actualDimensions = new ArrayList<>();
        // Récupérer les textes des onglets
        for (WebElement dimension : productsDimensions) {
            String dimensionText = dimension.getText().trim();
            if (!dimensionText.isEmpty()) {
                actualDimensions.add(dimensionText);
            }
        }
        return actualDimensions;
    }

//    public List<WebElement> getProductQuantity() {
//        return productQuantity;
//    }

    public List<String> getProductQuantity() {
        // Récupérer tous les éléments de la liste
//        List<WebElement> quantities = productQuantity;
        // Créer une liste pour stocker les textes des éléments
        List<String> actualQuantities = new ArrayList<>();
        // Récupérer les textes des onglets
        for (WebElement quantity : productQuantity) {
            String quantityValue = quantity.getAttribute("value");
            actualQuantities.add(quantityValue);
        }
        return actualQuantities;
    }
//
//    public List<WebElement> getProductPrices() {
//        return productsPrices;
//    }

    public List<Double> getProductsPrices() {
        // Récupérer tous les éléments de la liste
//        List<WebElement> prices = productsPrices;
        // Créer une liste pour stocker les textes des éléments
        List<Double> actualPrices = new ArrayList<>();
        // Récupérer les textes des onglets
        for (WebElement price : productsPrices) {
            String priceString = price.getText().trim().substring(0, price.getText().length() - 1).replace(",", ".");
            Double priceDouble = Double.parseDouble(priceString);
            actualPrices.add(priceDouble);
        }
        return actualPrices;
    }

    public String getProductsQuantity() {
        return productsQuantity.getText();
    }

    public Double getSubTotal() {
        String subTotalString = subTotal.getText().substring(0, subTotal.getText().length() - 1).replace(",", ".");
        return Double.parseDouble(subTotalString);
    }

    public Double getTotalTTC() {
        String totalTTCString = totalTTC.getText().substring(0, totalTTC.getText().length() - 1).replace(",", ".");
        return Double.parseDouble(totalTTCString);
    }
}
