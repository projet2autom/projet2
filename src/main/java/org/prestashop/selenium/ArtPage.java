package org.prestashop.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ArtPage extends AbstractPage {
    public ArtPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h2[@class='h3 product-title']/a")
    private List<WebElement> artProductsList;

    @FindBy(xpath = "//p[@class='text-uppercase h6 hidden-sm-down']")
    private WebElement filtrerPar;

    @FindBy(xpath = "//a[normalize-space()='Affiche encadrée The best...']")
    private WebElement bestProduct;

    // Méthode de clic sur AFFICHE ENCADRÉE THE BEST IS YET TO COME pour accéder à la page de produit
    public BestProductPage clickBestProduct(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, bestProduct);
        return new BestProductPage(driver);
    }

    // Getters
    public List<WebElement> getArtProductsList() {
        return artProductsList;
    }

    public WebElement getFiltrerPar(WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(filtrerPar));
    }
}
