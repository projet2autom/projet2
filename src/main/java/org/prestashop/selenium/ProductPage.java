package org.prestashop.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductPage extends AbstractPage {

    @FindBy(xpath = "//button[contains(@class,'btn-comment') and i]")
    protected WebElement firstComment;

    @FindBy(xpath = "//input[@id='comment_title']")
    protected WebElement commentTitleField;

    @FindBy(xpath = "//textarea[@id='comment_content']")
    protected WebElement commentBodyField;

    @FindBy(xpath = "//*[@type=\"submit\" and parent::div[contains(@class,'comment')]]")
    protected WebElement envoyerCommentaire;

    @FindBy(xpath = "//p[@class='h2' and ancestor::div[@id='product-comment-posted-modal']]")
    protected WebElement avisEnvoyer;

    @FindBy(xpath = "//div[@class='comments-nb']")
    protected WebElement nbrCommentaire;

    @FindBy(xpath = "//button[parent::div/preceding-sibling::div[@id=\"product-comment-posted-modal-message\"]]")
    protected WebElement fermerAvis;

    @FindBy(xpath = "//p[@class='h4']")
    protected WebElement commentTitle;

    @FindBy(xpath = "//p[preceding-sibling::p[@class='h4']]")
    protected WebElement commentContent;

    @FindBy(xpath="//div[@id='empty-product-comment']")
    protected WebElement noComment;

    @FindBy(xpath="//button[@data-button-action=\"add-to-cart\"]")
    protected WebElement addToCart;

    @FindBy(xpath="//span[@class=\"current-price-value\"]")
    protected WebElement currentPrice;

    protected ProductPage(WebDriver driver) {
        super(driver);
    }

    public void premierCommentaireClick(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, firstComment);
    }

    public void commentaire(WebDriverWait wait, String titre, String commentaire) {
        seleniumTools.sendKey(wait, commentTitleField, titre);
        seleniumTools.sendKey(wait, commentBodyField, commentaire);
    }

    public void envoyerCommentaire(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, envoyerCommentaire);
    }

    public void selectStar(WebDriverWait wait, String nbrStar) throws InterruptedException {
        String path = String.format("(//div[contains(@class,'star') and parent::div[contains(@class,'clearfix')]])[%s]", nbrStar);
        WebElement we = driver.findElement(By.xpath(path));
        seleniumTools.clickOnElement(wait, we);
    }

    public String numberOfComment() {
        String weTxt = nbrCommentaire.getText();

        System.out.println("texte commentaire : " + weTxt);

        Pattern pattern = Pattern.compile("Commentaires \\((\\d+)\\)");
        Matcher matcher = pattern.matcher(weTxt);

        if (matcher.find()) {
            return matcher.group(1);
        }

        return "group not find";
    }


    public boolean avisPoster(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(avisEnvoyer));
        return avisEnvoyer.isDisplayed();
    }

    public void fermerAvis(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait, fermerAvis);
    }

    public String getCommentTitle(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(commentTitle));
        return commentTitle.getText();
    }

    public String getCommentContent(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(commentContent));
        return commentContent.getText();
    }

    public String getCommentGrade(){
        List<WebElement> commentGrade = driver.findElements(By.xpath("//div[@class='star-on' and ancestor::div[@class='product-comment-list-item row']]"));
        int grade = 0;
        for(WebElement we : commentGrade){
            grade++;
        }
        return String.format("%s",grade);
    }

    public boolean commentVisible(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(noComment));
        return !noComment.isDisplayed();
    }

    public PopUpRecapPage addProductToCart(WebDriverWait wait) throws InterruptedException {
        seleniumTools.clickOnElement(wait,addToCart);
        return new PopUpRecapPage(driver);
    }

    public String getCurrentPrice(){
        return currentPrice.getAttribute("content");
    }

}
