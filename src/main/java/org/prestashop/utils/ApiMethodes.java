package org.prestashop.utils;
import io.restassured.response.Response;

import java.util.Base64;

import static io.restassured.RestAssured.given;


public class ApiMethodes {
    private final String key ="1TU6EC8K8LBFV9Q7812EE4YTTZJQLSPB";
    private final String baseUrl = "http://192.168.102.174:8081/api";

    public Response apiGetWithFilter(String filter, String param){

        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .param(filter,param)
                .when()
                .get("/customers")
                .then().
                 extract().response();
        return  resp;
    }

    public Response getAllCustomers(){
        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .when()
                .get("/customers")
                .then().
                extract().response();
        return  resp;
    }

    public Response postCustomer(String firstname, String lastname, String mail,String pwd, String active, String birth){

        String stringbody =String.format("<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                "  <customer>\n" +
                "    <passwd>%s</passwd>\n" +
                "    <lastname>%s</lastname>\n" +
                "    <firstname>%s</firstname>\n" +
                "    <email>%s</email>\n" +
                "   <active>%s</active>\n" +
                "   <id_gender>1</id_gender>\n"+
                "   <optin>1</optin>\n"+
                "    <birthday>%s</birthday>\n" +
                "    <newsletter>1</newsletter>\n"+
                "    <associations>\n" +
                "      <groups>\n" +
                "        <group>\n" +
                "          <id>3</id>\n" +
                "        </group>\n" +
                "      </groups>\n" +
                "    </associations>"+
                "  </customer>\n" +
                "</prestashop>",
                pwd,lastname,firstname,mail,active,birth);

        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .body(stringbody)
                .when()
                .post("/customers")
                .then()
                .extract().response();
        return resp;

    }

    public Response getCustomerWithId(String id){
        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .when()
                .get("/customers/"+id)
                .then().
                extract().response();
        return  resp;
    }
    public Response updateCustomerStatus(String id, String active){
        String stringbody =String.format("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                        "    <customer>\n" +
                        "        <id>%s</id>\n" +
                        "        <active>%s</active>\n" +
                        "    </customer>\n" +
                        "</prestashop>",
                id,active);


        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .body(stringbody)
                .when()
                .patch("/customers/"+id)
                .then().
                extract().response();
        return  resp;
    }

    public Response deleteCustomer(String id){

        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .when()
                .delete("/customers/"+id)
                .then().
                extract().response();
        return  resp;
    }

    public Response getProductNoRightsKey(){
        Response resp = given()
                .auth().basic(key,"")
                .baseUri(baseUrl)
                .when()
                .delete("/products")
                .then().
                extract().response();
        return  resp;
    }

    public Response getProductwithoutKey(){
        Response resp = given()
                .baseUri(baseUrl)
                .when()
                .delete("/products")
                .then().
                extract().response();
        return  resp;
    }

}

