package org.prestashop.utils;


import java.sql.*;
import java.util.*;

public class DBTools {

    private final String user = "presta_user";
    private final String pwd = "presta_password";
    private final String dbms = "mysql";
    private final String serverName = "192.168.102.174";
    private final String portNumber = "3308";
    private final String dbUrl = String.format("jdbc:%s://%s:%s/prestashop", dbms, serverName, portNumber);


    private Connection connection = null;

    public Connection getConnection() throws SQLException {


        Properties connectionProps = new Properties();
        connectionProps.put("user", this.user);
        connectionProps.put("password", this.pwd);

        System.out.println("Properties set.\ntry to connect...");

        connection = DriverManager.getConnection(dbUrl, connectionProps);

        System.out.println("Connected to database !");

        return connection;
    }

    public Map<String,String> query(String query) {
        Map<String,String> result = new HashMap<>();

        try (Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(query);
        ) {
            rs.next();

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
//            System.out.println(columnCount+" column in result");

            for(int i=1;i<=columnCount;i++){
//                System.out.print("get col name "+i+" : ");
                String colName = rsmd.getColumnName(i);
//                System.out.println(colName);
                String val = rs.getString(colName);
                result.put(colName,val);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void updateDB(String query){
        int affectedRows = 0;

        try (var pstmt = connection.prepareStatement(query)) {

            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteRow(String query) {
        try(var dstmt = connection.prepareStatement(query)){
            dstmt.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
}
