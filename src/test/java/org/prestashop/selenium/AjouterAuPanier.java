package org.prestashop.selenium;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;


public class AjouterAuPanier extends AbstractTest {

    static final Properties properties = new Properties();

    static {
        try {
            InputStream fileAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(String.format("data_test/%s.properties", Connection.class.getSimpleName()));
            properties.load(fileAsStream);
        } catch (IOException e) {
            //e.printStackTrace();
            throw new ExceptionInInitializerError(e);
        }
    }

    final String BROWSER_URL = properties.getProperty("url");
    final String USERNAME = properties.getProperty("username");
    final String PASS = properties.getProperty("password");

//    final String ADDRESS = properties.getProperty("address");
//    final String POSTCODE = properties.getProperty("postcode");
//    final String CITY = properties.getProperty("city");


    @Test
    public void ajouterAuPanierTest() throws InterruptedException {
        // 1. Accéder à l'application Prestashop
        // Accéder à l'application Prestashop
        logger.info("Start of the Connection's Test");
        logger.info("Get url {}", BROWSER_URL);

        driver.get(BROWSER_URL);

        logger.info("Access Welcome Page");
        WelcomePage welcomePage = new WelcomePage(driver);
        HeadBand headBand = new HeadBand(driver);

        // Vérifier: La page d'accueil de Prestashop s'affiche.
            // Des produits sont présents sur la page avec un menu défilant de produits.
        WebElement produitsPopulaires = WelcomePage.getProduitsPopulaires();
        assertTrue(produitsPopulaires.isDisplayed());

        List<WebElement> productsList = welcomePage.getProductsList();
        assertFalse(productsList.isEmpty());

            //L'utilisateur est déconnecté et le panier est vide.
        WebElement deconnexion = headBand.getConnexionTxt(wait);
        assertEquals("Connexion",deconnexion.getText());

        WebElement panierVide = headBand.getPanier(wait);
        assertEquals("(0)",panierVide.getText());

        // Appel de la méthode clickConnexion pour accéder à la page Connection
        ConnectionPage connectionPage = headBand.clickbtnConnexion(wait) ;

        logger.info("Access Connection page");
        // Appel de la méthode

        // 2. Se connecter avec ${email} et le mot de passe ${mdp}
        // Appel de la méthode fillUsername
        connectionPage.fillUsername(wait, USERNAME);
        // Appel de la méthode fillPassword
        connectionPage.fillPassword(wait, PASS);

        logger.info("Connected as {}", USERNAME);
        // Appel de la méthode clickConnexion pour accéder à la page Votre compte
        AccountPage accountPage = connectionPage.clickConnexion(wait);

        // Vérifier: L'utilisateur ${user} est connecté avec son nom prénom en haut.
        // Le bouton déconnexion est affiché
        assertEquals("John DOE", headBand.getConnectedUser(wait));
        assertTrue(headBand.getDeconnexion(wait).getText().contains("Déconnexion"));
        logger.info("Connection's Test done");

        // 3. Cliquer sur la catégorie 'Art'
        // Appel de la méthode clickArt pour accéder à la page Art
        ArtPage artPage = headBand.clickArt(wait);

        // Vérifier:
        // La catégorie Art s'affiche avec au centre :
        // une sous catégorie sauf pour l'art ???
        // les produits disponibles
        // A gauche, nous avons : Encart 'Filtrer par'
        List<WebElement> artProductsList = artPage.getArtProductsList();
        assertTrue(!artProductsList.isEmpty());

        WebElement trierPar = artPage.getFiltrerPar(wait);
        assertTrue(trierPar.isDisplayed());

        logger.info("Access Art Page");

        // 4. Cliquer sur l'affiche 'AFFICHE ENCADRÉE THE BEST IS YET TO COME'
        // Appel de la méthode clickBestProduct pour accéder à la page de produit
        BestProductPage bestProductPage = artPage.clickBestProduct(wait);

        // Vérifier: La page du produit s'affiche avec le prix 34,80€
        assertEquals("AFFICHE ENCADRÉE THE BEST IS YET TO COME", bestProductPage.getProductName(wait));
        assertEquals("34,80 €", bestProductPage.getProductPrice(wait));

        logger.info("Access AFFICHE ENCADRÉE THE BEST IS YET TO COME Page");

        // 5. Ajouter au panier 2 affiches 40x60cm
        // Appel de la méthode clickAddQuantity
        bestProductPage.clickAddQuantity(wait);

        // Appel de la méthode clickAjouterAuPanier
        bestProductPage.clickAjouterAuPanier(wait);

        // Vérifier: Le popup 'Produit ajouté au panier avec succès' apparaît avec 2 articles dans le panier.
        // Le transport est gratuit.
        // Le sous-total et le total TTC sont corrects.
        WebElement popUp = bestProductPage.getPopupProductAdded(wait);
        assertTrue(popUp.isDisplayed());

        System.out.println("Popup displayed");

        String numberArticles40 = bestProductPage.getNumberArticles(wait);
        assertEquals("Il y a 2 articles dans votre panier.",numberArticles40);

        String transport = bestProductPage.getTransport();
        assertEquals("gratuit",transport);

        String unitPriceString = bestProductPage.getUnitPriceString();
        assertEquals("34,80 €",unitPriceString);

        Double unitPrice40 = bestProductPage.getUnitPrice();
        Double subTotal40 = bestProductPage.getSubTotal();
        Double totalTTC40 = bestProductPage.getTotalTTC();
        assertEquals(2*unitPrice40,subTotal40,0.001);
        assertEquals(2*unitPrice40,totalTTC40,0.001);

        System.out.println("2 affiches 40x60cm added");

        // 6. Cliquer sur continuer mes achats
        // Appel de la méthode clickContinuerAchats
        bestProductPage.clickContinuerAchats(wait);

        System.out.println("Back to product page");

        // Vérifier: Le popup disparaît et la page du produit est toujours visible. Le panier montre 2 articles
        //assertTrue(!popUp.isDisplayed());
        assertEquals("AFFICHE ENCADRÉE THE BEST IS YET TO COME", bestProductPage.getProductName(wait));

        WebElement panier2 = headBand.getPanier(wait);
        assertEquals("(2)",panier2.getText());

        //7. Ajouter 1 affiche 60x90cm après avoir vérifier que le prix soit égale à 58,80€ sur la page produit
        // Appel de la méthode selectDimension60
        bestProductPage.selectDimension60(wait);

        assertEquals("58,80 €", bestProductPage.getProductPrice(wait));

        // Appel de la méthode clickAjouterAuPanier
        bestProductPage.clickAjouterAuPanier(wait);

        // Vérifier: Le popup 'Produit ajouté au panier avec succès' apparaît avec 3 articles dans le panier.
        //Le sous-total et le total TTC sont corrects.
        //WebElement popUp2 = bestProductPage.getPopupProductAdded(wait);
        assertTrue(popUp.isDisplayed());

        String numberArticles60 = bestProductPage.getNumberArticles(wait);
        assertEquals("Il y a 3 articles dans votre panier.",numberArticles60);

        Double unitPrice60 = bestProductPage.getUnitPrice();
        Double subTotal60 = bestProductPage.getSubTotal();
        Double totalTTC60 = bestProductPage.getTotalTTC();
        assertEquals(unitPrice60+subTotal40,subTotal60,0.001);
        assertEquals(unitPrice60+totalTTC40,totalTTC60,0.001);

        System.out.println("1 affiche 60x90cm added");

        // Appel de la méthode clickContinuerAchats //??? Page manquante dans case de test
        bestProductPage.clickContinuerAchats(wait);

        System.out.println("Back to product page");

        // 8. Ajouter 1 affiche 80x120cm après avoir vérifier que le prix soit égale à 94,80 sur la page produit
        // Appel de la méthode selectDimension80
        bestProductPage.selectDimension80(wait);

        assertEquals("94,80 €", bestProductPage.getProductPrice(wait));

        // Appel de la méthode clickAjouterAuPanier
        bestProductPage.clickAjouterAuPanier(wait);

        // Vérifier: Le popup 'Produit ajouté au panier avec succès' apparaît avec 4 articles dans le panier.
        //Le sous-total et le total TTC sont corrects.
        //WebElement popUp3 = bestProductPage.getPopupProductAdded(wait);
        assertTrue(popUp.isDisplayed());

        String numberArticles80 = bestProductPage.getNumberArticles(wait);
        assertEquals("Il y a 4 articles dans votre panier.",numberArticles80);

        Double unitPrice80 = bestProductPage.getUnitPrice();
        Double subTotal80 = bestProductPage.getSubTotal();
        Double totalTTC80 = bestProductPage.getTotalTTC();
        assertEquals(unitPrice80+subTotal60,subTotal80,0.001);
        assertEquals(unitPrice80+totalTTC60,totalTTC80,0.001);

        System.out.println("1 affiche 90x120cm added");

        // 9. Cliquer sur 'Commander'
        // Appel de la méthode clickCommander
        CartPage cartPage = bestProductPage.clickCommander(wait);

        System.out.println("Access to Cart Page");

        // Vérifier: La page du panier s'affiche.
        //Les 4 affiches sont visibles. Le prix unitaire, le nombre de produit et le total sont corrects.
        //Le recapitulatif du panier est correct.
//        String product40 = cartPage.getProductDimensions().get(0).getText();
//        assertEquals("40x60cm", product40);
        List<String> expectedProductsDimensions4 = Arrays.asList(
                "40x60cm",
                "60x90cm",
                "80x120cm"
        );
        List<String> actualProductsDimensions4 = cartPage.getProductDimensions();
        assertEquals(expectedProductsDimensions4, actualProductsDimensions4);
        System.out.println("3 types of products in the cart");

        List<String> expectedQuantities4 = Arrays.asList(
                "2",
                "1",
                "1"
        );
        List<String> actualQuantities4  = cartPage.getProductQuantity();
        assertEquals(expectedQuantities4,actualQuantities4);
        System.out.println("4 items in the cart");

        List<Double> expectedPrices4 = Arrays.asList(
                2*unitPrice40,
                1*unitPrice60,
                1*unitPrice80
        );
        List<Double> actualPrices4 = cartPage.getProductsPrices();
        assertEquals(expectedPrices4, actualPrices4);

        String productsQuantity4 = cartPage.getProductsQuantity();
        assertEquals("4 articles",productsQuantity4);

        Double subTotal4 = cartPage.getSubTotal();
        Double totalTTC4 = cartPage.getTotalTTC();
        assertEquals(2*unitPrice40+unitPrice60+unitPrice80,subTotal4,0.001);
        assertEquals(2*unitPrice40+unitPrice60+unitPrice80,totalTTC4,0.001);
        System.out.println("Sub-total and Total TTC of 4 items are corrects");

        // 10. Supprimer l'affiche 80x120cm et vérifier le panier
        // Appel de la méthode clickDeleteProduct80
        cartPage.clickDeleteProduct80(wait);

        // Vérifier: L'affiche 80x120cm est bien supprimé et le récapitulatif du panier est correct
        List<String> expectedProductsDimensions3 = Arrays.asList(
                "40x60cm",
                "60x90cm"
        );
        List<String> actualProductsDimensions3 = cartPage.getProductDimensions();
        assertEquals(expectedProductsDimensions3, actualProductsDimensions3);
        System.out.println("2 types of products in the cart, exclude 80x120cm");

        List<String> expectedQuantities3 = Arrays.asList(
                "2",
                "1"
        );
        List<String> actualQuantities3  = cartPage.getProductQuantity();
        assertEquals(expectedQuantities3, actualQuantities3);
        System.out.println("3 items in the cart");

        String productsQuantity3 = cartPage.getProductsQuantity();
        assertEquals("3 articles",productsQuantity3);

        Double subTotal3 = cartPage.getSubTotal();
        Double totalTTC3 = cartPage.getTotalTTC();
        assertEquals(2*unitPrice40+unitPrice60,subTotal3,0.001);
        assertEquals(2*unitPrice40+unitPrice60,totalTTC3,0.001);
        System.out.println("Sub-total and Total TTC of 4 items are corrects");

        // 11. Valider le panier en cliquant sur 'commander'
        // Appel de la méthode clickCommander
        OrderPage orderPage = cartPage.clickCommander(wait);

        // Vérifier: La page des informations personnelles s'affichent
        assertTrue(orderPage.getPageInfoTitle(wait).contains("INFORMATIONS PERSONNELLES"));
        System.out.println("Access to Personnal informations");

        // 12. Sélectionner l'adresse Française et cliquer sur continuer
        // Appel de la méthode clickFrenchRadio
        orderPage.clickFrenchRadio(wait);
        //orderPage.selectAddress(wait, ADDRESS, POSTCODE, CITY);

        // Appel de la méthode clickContinuer1
        orderPage.clickContinuer1(wait);

        // Vérifier: Le mode de livraison s'affiche
        assertEquals("3\nMODE DE LIVRAISON", orderPage.getPageDeliveryTitle(wait));
        System.out.println("Access to Delivery modals");

        // 13. Selectionner 'Retrait en magasin' et continuer
        // Appel de la méthode clickShopCollect
        orderPage.clickShopCollect(wait);

        // Appel de la méthode clickContinuer2
        orderPage.clickContinuer2(wait);

        // Vérifier: La page de paiement s'affiche
        assertEquals("4\nPAIEMENT", orderPage.getPagePaymentTitle(wait));
        System.out.println("Access to Payment");

        // 14. Cliquer sur 'Payer par virement bancaire', cocher 'J'ai lu les conditions générales de vente et j'y adhère sans réserve.' et commander
        // Appel de la méthode clickPayByCard
        orderPage.clickPayByCard(wait);

        // Appel de la méthode clickConditionsGenerales
        orderPage.clickConditionsGenerales(wait);

        // Appel de la méthode clickCommander
        orderPage.clickCommander(wait);

        // Vérifier: La page de confirmation de commande s'affiche avec :
        //VOTRE COMMANDE EST CONFIRMÉE
        assertEquals("\uE876VOTRE COMMANDE EST CONFIRMÉE", orderPage.getPageConfirmTitle(wait));
        //Les 3 articles avec prix unitaires, quantité et total produits
        List<String> expectedOrder40 = Arrays.asList(
                "34,80 €",
                "2",
                "69,60 €"
        );
        assertEquals(expectedOrder40, orderPage.getOrder40());

        List<String> expectedCommande60 = Arrays.asList(
                "58,80 €",
                "1",
                "58,80 €"
        );
        assertEquals(expectedCommande60, orderPage.getOrder60());

        //le total TTC
        assertEquals(2*unitPrice40+unitPrice60,orderPage.getTotalTTC(),0.001);

        //Le panier en haut à gauche est vide
        assertEquals("(0)",panierVide.getText());

        System.out.println("Order confirmed");

    }
}
