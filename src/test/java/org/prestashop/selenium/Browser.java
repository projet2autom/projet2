package org.prestashop.selenium;

public enum Browser {
    EDGE,
    FIREFOX,
    CHROME;
}
