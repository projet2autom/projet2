package org.prestashop.selenium;

import io.restassured.response.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;
import org.prestashop.utils.ApiMethodes;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class VerificationInformationsPersonnelles extends AbstractTest{

    static final Properties properties = new Properties();

    static {
        try {
            InputStream fileAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(String.format("data_test/%s.properties", Connection.class.getSimpleName()));
            properties.load(fileAsStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ExceptionInInitializerError(e);
        }
    }

    final String BROWSER_URL = properties.getProperty("url");
     String USERNAME = "usb1@gmail.com";
     String PASS = "prestashop";
     String FIRSTNAME = "John";
     String LASTNAME = "DOE";
     String NEWPWD = "prestashop2";
    String BIRTHDAY="1970-01-15";

    String ID="";
    @BeforeEach
    public void creationUtilisateur(){
        System.out.println("Créer client");
        ApiMethodes api= new ApiMethodes();
        Response resp= api.postCustomer(FIRSTNAME,LASTNAME,USERNAME,PASS,"1",BIRTHDAY);
        System.out.println(resp.prettyPrint());
        ID= resp.path("prestashop.customer.id").toString();
        System.out.println(ID);
    }


    @Test
    public void verificationInformationPersonnellesTest() throws InterruptedException, ParseException {
        System.out.println("***pas 1- Accéder à l'application Prestashop***");
        System.out.println("Accéder à l'url");
        driver.get(BROWSER_URL);

        System.out.println("la page d'accueil s'affiche");
        //initialisation de la Page d'accueil
        WelcomePage welcomePage = new WelcomePage(driver);
        //Assert sur le titre
        assertEquals(driver.getTitle(),"prestashop");

        System.out.println("Des produits sont présents avec un menu de produits");
        //assert sur le carroussel
        WebElement caroussel = welcomePage.caroussel;
        assertTrue(caroussel.isDisplayed());

        //assert sur la liste des produits - liste non vide
        List<WebElement> productsList = welcomePage.getProductsList();
        assertFalse(productsList.isEmpty());

        System.out.println("*** pas 2 - cliquer sur Information personnelles dans le footer");
        ConnectionPage connectionPage= welcomePage.clickConnexionFooter(wait);

        System.out.println("Assert sur la page de connexion (présence des champs)");
        assertTrue(connectionPage.getUsernameField().isDisplayed(),"Le champ utilisateur n'est pas présent");
        assertTrue(connectionPage.getPwdField().isDisplayed(),"Le champ mot de passe n'est pas visible");
        assertTrue(connectionPage.getShowButton().isDisplayed(),"le boutton montrer mot de passe n'est pas visible");
        assertTrue(connectionPage.getSubmitButton().isDisplayed(),"le boutton Connexion n'est pas visible");
        assertTrue(connectionPage.getForgottenPwd().isDisplayed(),"Le lien Mot de passe oublié n'est pas visible");
        assertTrue(connectionPage.getCreateAccountButton().isDisplayed(),"Le lien Créer un compte n'est pas visible");

        System.out.println("***pas 3 - Renseigner le mail et le mot de passe ");
        //remplir le champ utilisateur
        connectionPage.fillUsername(wait,USERNAME);

        //remplir le champ mdp
        connectionPage.fillPassword(wait,PASS);

        //vérifier que le champ username est bien rempli
        String username = connectionPage.getUsernameField().getAttribute("value");
        System.out.println("Ceci est le username champ: "+username);
        assertEquals(username, USERNAME);

        //Vérifier que le champ password est bien caché - via l'attribut type
        String pwd = connectionPage.getPwdField().getAttribute("type");
        assertEquals("password", pwd,"L'attribut doit être de type password quand le mot de passe est caché");

        System.out.println("***pas 4- Cliquer sur Afficher***");
        //click sur le bouton montrer du champ password
        connectionPage.getShowButton().click();

        System.out.println("Vérifier que le mot de passe s'affiche correctement");
        String pwd_clear = connectionPage.getPwdField().getAttribute("type");
        assertEquals("text",pwd_clear,"L'attribut doit être de type text  quand le mot de passe est caché");

        //assert sur la valeur du mot de passe
        //on récupère la valeur insérée
        String pwd_value = connectionPage.getPwdField().getAttribute("value");
        System.out.println("Ceci est le username cpwd: "+pwd_value);
        //assert entre le mot de passe renseigné dans le champ et celui qu'on a en variable
        assertEquals(pwd_value,PASS,"le mot de passe doit être celui référencé dans la variable PASS");

        System.out.println("***pas 5- Cliquer sur se connecter ***");
        //click et initatialisation BoardAccount
        AccountBoard accountBoard = connectionPage.clickConnexionFooter(wait);

        System.out.println("le tableau de bord du compte s'affiche- assert");
        //assert sur le titre
        assertEquals("Mon compte",driver.getTitle(),"le titre de la page visée - mon compte- ne correspond pas à ce qu'on a");


        System.out.println("***pas 3 -Cliquer sur information");
        AccountPage accountPage = accountBoard.clickInformation(wait);

        System.out.println("Assert sur les informations personnelles disponibles");
        assertEquals(accountPage.getFirstnameField().getAttribute("value"),FIRSTNAME,"Le prénom n'est pas celui renseigné dans la varibale FIRSTNAME");
        assertEquals(accountPage.getLastnameField().getAttribute("value"),LASTNAME,"Le nom n'est pas celui renseigné dans la varibale LASTNAME");
        assertEquals(accountPage.getBirthdayField().getAttribute("value"),formatDate(BIRTHDAY), "La date de naissance n'est pas la bonne");  //pour un assert sur la date il faut changer le format
        assertEquals(accountPage.getEmailField().getAttribute("value"),USERNAME,"Le champ email n'est pas celui renseigné dans la variable USERNAME");
        assertEquals(accountPage.getOldPasswordField().getAttribute("value"),"","Le champ password doit être vide");
        assertTrue(accountPage.getCheckbox_M().isSelected(),"La checkbox M doit être sélectionnée");
        assertTrue(accountPage.getNewsletterCheckbox().isSelected(),"La checkbox newsletter doit être sélectionnée");
        assertTrue(accountPage.getOptinChexbox().isSelected(),"La checkbox opt in doit être sélectionnée");

        

        System.out.println("***pas 6-Renseigner le nouveau mot de passe***");
        System.out.println("Renseigner l'ancien mot de passe");
        accountPage.fillOldPassword(wait,PASS);
        //vérifier que l'ancien mot de passe est caché
        String verifOldPwdcache =accountPage.getOldPasswordField().getAttribute("type");
        assertEquals(verifOldPwdcache,"password","Le type devrait être de password- car il devrait être caché");

        System.out.println("asset remplissage ancien mdp");
        assertFalse(accountPage.getOldPasswordField().getAttribute("value").isEmpty());

        

        System.out.println("Renseigner le nouveau mot de passe");
        accountPage.fillNewPassword(wait,NEWPWD);
        //vérifier que le nouveau mdp est caché
        String verifNewPwd = accountPage.getNewPasswordField().getAttribute("type");
        assertEquals(verifNewPwd,"password","Le type devrait être de password- car il devrait être caché");

        System.out.println("assert bon remplissage champ nouveau mdp");
        assertFalse(accountPage.getNewPasswordField().getAttribute("value").isEmpty());

        System.out.println("*** pas 7- Afficher les deux mots de passe");
        System.out.println("Affichage de l'ancien mdp");
        accountPage.clickShowOldPassword(wait);
        assertEquals(accountPage.getOldPasswordField().getAttribute("value"),PASS, "l'ancien mot de passe n'est pas le bon");
        
        System.out.println("Affichage du nouveau mdp");
        accountPage.clickShowNewPassword(wait);
        assertEquals(accountPage.getNewPasswordField().getAttribute("value"),NEWPWD, "le nouveau mot de passe n'est pas le bon");
        
        System.out.println("***pas 8- décocher newsletter ***");
        accountPage.uncheckChekbox(accountPage.getNewsletterCheckbox());
        //assert sur la désélection
        assertFalse(accountPage.getNewsletterCheckbox().isSelected());

        
        System.out.println("*** pas 8- cocher mentions oblogatoires");
        System.out.println("sélectionner condition générales");
        accountPage.checkChekbox(accountPage.getConditionGenerales());
        //assert sur la case cochée
        assertTrue(accountPage.getConditionGenerales().isSelected());
        
        System.out.println("sélectionner confidentialité des données");
        accountPage.checkChekbox(accountPage.getCustomerPrivacy());
        //assert sur la case cochée
        assertTrue(accountPage.getCustomerPrivacy().isSelected());
        
        System.out.println("*** pas 9- cliquer sur Enregistrer");
        //click
        accountPage.clickButtonEnregistrer(wait);
        //assert sur l'apparition d'un message qui indique que les données ont bien été enregistrées
        assertTrue(accountPage.getAlerteSucces().isDisplayed());
    
        System.out.println("Assert sur les nouvelles données personnelles enregistrées");
        assertEquals(accountPage.getFirstnameField().getAttribute("value"),FIRSTNAME,"Le prénom n'est pas celui renseigné dans la varibale FIRSTNAME");
        assertEquals(accountPage.getLastnameField().getAttribute("value"),LASTNAME,"Le nom n'est pas celui renseigné dans la varibale LASTNAME");
        assertEquals(accountPage.getBirthdayField().getAttribute("value"),formatDate(BIRTHDAY), "La date de naissance n'est pas la bonne");
        assertEquals(accountPage.getEmailField().getAttribute("value"),USERNAME,"Le champ email n'est pas celui renseigné dans la variable USERNAME");
        assertEquals(accountPage.getOldPasswordField().getAttribute("value"),"","Le champ password doit être vide");
        assertTrue(accountPage.getCheckbox_M().isSelected(),"La checkbox M doit être sélectionnée");
        assertFalse(accountPage.getNewsletterCheckbox().isSelected(),"La checkbox newsletter ne doit pas être sélectionnée");
        assertTrue(accountPage.getOptinChexbox().isSelected(),"La checkbox opt in doit être sélectionnée");
    }

    @AfterEach
    public void tearDownDeleteUser(){
        ApiMethodes api= new ApiMethodes();
        Response deletion = api.deleteCustomer(ID);
        deletion.then().assertThat().statusCode(200);
        System.out.println("l'utilisateur a bien été supprimé");


   }
}
