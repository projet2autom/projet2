package org.prestashop.selenium;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RechercherProduit extends AbstractTest {
    static final Properties properties = new Properties();

    static {
        try {
            InputStream fileAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(String.format("data_test/%s.properties", Connection.class.getSimpleName()));
            properties.load(fileAsStream);
        } catch (IOException e) {
            //e.printStackTrace();
            throw new ExceptionInInitializerError(e);
        }
    }

    final String BROWSER_URL = properties.getProperty("url");
    final String KEYWORD = properties.getProperty("keyword");

    @Test
    public void rechercherProduitTest() throws InterruptedException {
        // 1. Accéder à l'application Prestashop
        // Accéder à l'application Prestashop
        logger.info("Start of the Connection's Test");
        logger.info("Get url {}", BROWSER_URL);

        driver.get(BROWSER_URL);

        logger.info("Access Welcome Page");
        WelcomePage welcomePage = new WelcomePage(driver);
        HeadBand headBand = new HeadBand(driver);

        // Vérifier: La page d'accueil de Prestashop s'affiche.
        // Des produits sont présents sur la page avec un menu défilant de produits.
        WebElement produitsPopulaires = WelcomePage.getProduitsPopulaires();
        assertTrue(produitsPopulaires.isDisplayed());

        List<WebElement> productsList = welcomePage.getProductsList();
        assertFalse(productsList.isEmpty());

        //L'utilisateur est déconnecté et le panier est vide.
        WebElement deconnexion = headBand.getConnexionTxt(wait);
        assertEquals("Connexion", deconnexion.getText());

        WebElement panierVide = headBand.getPanier(wait);
        assertEquals("(0)", panierVide.getText());

        // 2. Dans le champ 'Recherche', renseigner le produit : Mug Today Is A Good Day
        // Appel de la méthode searchByKeyword
        MugSearchResultPage mugSearchResultPage = headBand.searchByKeyword(wait, KEYWORD);

        // Vérifier: La liste des produits correspondants s'affichent. Elle contient le produit
        assertTrue(mugSearchResultPage.getMugsList().contains(KEYWORD));

        // 3. Cliquer sur le produit
        //Appel de la méthode clickMugSearched
        MugSearchedPage mugSearchedPage = mugSearchResultPage.clickMugSearched(wait);

        // Vérifier: La page du produit s'affiche
        assertEquals("MUG TODAY IS A GOOD DAY", mugSearchedPage.mugName.getText());
    }
}
