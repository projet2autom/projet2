package org.prestashop.selenium;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chromium.ChromiumOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.AbstractDriverOptions;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class AbstractTest {
    protected final Logger logger = LogManager.getLogger(AbstractTest.this);
    protected static WebDriver driver;
    protected WebDriverWait wait;

    private final Duration implicitWait = Duration.ofSeconds(1);
    private final Duration explicitWait = Duration.ofSeconds(3);

    private final String browser = System.getProperty("browser", "chrome");

    /**
     * Setup du driver en fonction du navigateur. On crée un enum car il n'existe que 3 navigateurs (on peut rajouter si on veut IE)
     */
    @BeforeEach
    void setup() {
        logger.info("Setup driver with browser : {}", browser);
        System.out.println(System.getProperty("os.name"));
        switch (Browser.valueOf(browser.toUpperCase())) {
            case CHROME:
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--remote-allow-origins=*");
                setupHeadless(chromeOptions);
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(chromeOptions);
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                if (System.getProperty("os.name").equalsIgnoreCase("linux")){
                    firefoxOptions.addArguments("--headless");
                }
                setupHeadless(firefoxOptions);
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case EDGE:
                EdgeOptions edgeOptions = new EdgeOptions();
                edgeOptions.addArguments("--remote-allow-origins=*");
                setupHeadless(edgeOptions);
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver(edgeOptions);
                break;
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(implicitWait);
        wait = new WebDriverWait(driver, explicitWait);
    }

    @AfterEach
    void teardown(){
        driver.close();
        driver.quit();
    }

    public <T extends AbstractDriverOptions> void setupHeadless(T options){
        if (System.getProperty("os.name").equalsIgnoreCase("linux")){
            if (options instanceof ChromiumOptions){
                ((ChromiumOptions) options).addArguments("--headless");
            } else if (options instanceof FirefoxOptions){
                ((FirefoxOptions) options).addArguments("--headless");
            } else {
                logger.error("Incorrect driver option");
            }
        }
    }
    public String formatDate(String dateOldFormat) throws ParseException {
        final String OLD_FORMAT = "yyyy-MM-dd";
        final String NEW_FORMAT = "dd/MM/yyyy";
        String newDateString;

        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        Date d = sdf.parse(dateOldFormat);
        sdf.applyPattern(NEW_FORMAT);
        return newDateString = sdf.format(d);
    }

}
