package org.prestashop.api;

import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.prestashop.selenium.AbstractTest;
import org.prestashop.utils.ApiMethodes;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AjouterClientNp extends AbstractTest {
    static final Properties properties = new Properties();

    String LASTNAME;
    String FIRSTNAME;
    String EMAIL;
    @Test
    public void testAjouterClientNp(){
        System.out.println("*** pas Rechercher les clients avec l'api customer");
        ApiMethodes api= new ApiMethodes();

        Response respAllcustomer = api.getAllCustomers();
        System.out.println("retour du get customer: "+ respAllcustomer.prettyPrint());

        System.out.println("Assert code de retour");
        respAllcustomer.then().assertThat().statusCode(200);

        System.out.println("Assert id client 1 et 2 présents");
        respAllcustomer.then().assertThat().body("prestashop.customers.customer.find { it.@id == '1' }", notNullValue());
        respAllcustomer.then().assertThat().body("prestashop.customers.customer.find { it.@id == '2' }", notNullValue());

        System.out.println("***pas 2- rechercher les informations du client id 1");
        Response respClient1= api.getCustomerWithId("1");
        System.out.println("Information client id 1: "+ respClient1.prettyPrint());

        System.out.println("Assert sur la présence des champs lastname, firstname, email et password et ces champs ne sont pas vides");
        respClient1.then().assertThat().body("prestashop.customer.lastname", not(emptyOrNullString()));
        respClient1.then().assertThat().body("prestashop.customer.firstname", not(emptyOrNullString()));
        respClient1.then().assertThat().body("prestashop.customer.email", not(emptyOrNullString()));
        respClient1.then().assertThat().body("prestashop.customer.passwd", not(emptyOrNullString()));

        System.out.println("***pas 3- creer un client égal à celui récupéré");
        //récupération des valeurs
        LASTNAME=respClient1.path("prestashop.customer.lastname").toString();
        FIRSTNAME=respClient1.path("prestashop.customer.firstname").toString();
        EMAIL=respClient1.path("prestashop.customer.email").toString();

        System.out.println("Méthode Post-Création d'un client identique");
        Response creation= api.postCustomer(FIRSTNAME,LASTNAME,EMAIL,"password","0","2015-05-12");

        System.out.println("body de retour du post  "+creation.prettyPrint());

        System.out.println("Assert code de retour 500");
        creation.then().assertThat().statusCode(500);

        System.out.println("Assert message email déjà utilisé");
        //extraction du message
        String msgError=creation.path("prestashop.errors.error.message[0]").toString();
        System.out.println("le message d'erreur "+msgError);
        assertEquals("Cet e-mail est déjà utilisé, veuillez en choisir un autre",msgError,"Le message d'erreur n'est pas le bon");








    }
}
