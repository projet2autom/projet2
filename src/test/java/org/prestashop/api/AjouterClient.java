package org.prestashop.api;
import org.prestashop.selenium.*;
import org.prestashop.utils.ApiMethodes;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class AjouterClient extends AbstractTest{
        static final Properties properties = new Properties();

        static {
                try {
                        InputStream fileAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(String.format("data_test/%s.properties", Connection.class.getSimpleName()));
                        properties.load(fileAsStream);
                } catch (IOException e) {
                        e.printStackTrace();
                        throw new ExceptionInInitializerError(e);
                }
        }
        final String URL = properties.getProperty("url");

        String EMAIL = "autom@gmail.com";
        String FIRSTNAME = "nini";
        String LASTNAME = "Baba";
        String PWD = "autom";
        String ID;

        @Test
        public void test() throws InterruptedException {
                ApiMethodes api = new ApiMethodes();




                System.out.println("****Pas 1- Vérifier qu'il n'y a pas de client ${firstname} ${lastname} avec l'api customers");

                System.out.println("vérifier le firstname");
                Response repFirstname = api.apiGetWithFilter("filter[firstname]","cust");
                //affichage du résultat en console
                System.out.println(repFirstname.prettyPrint());

                //Assert sur le body de retour
                //Assert sur le code de retour ainsi que sur l'absence de retour dans la balise customer
                repFirstname.then().assertThat().statusCode(200).body("prestashop.customers", equalTo(""));


                System.out.println("vérifier le lastname");
                Response repLastname = api.apiGetWithFilter("filter[lastname]","omer");
                //afficage du body de retour en console
                System.out.println(repLastname.prettyPrint());

                //Assert sur le code de retour ainsi que sur l'absence de retour dans la balise customer
                repLastname.then().assertThat().statusCode(200).body("prestashop.customers", equalTo(""));

                System.out.println("***Pas 2- Créer le client");
                //Envoyer dans la méthode pour créer un client avec nom, prénom, mail et password
                Response creation= api.postCustomer(FIRSTNAME, LASTNAME, EMAIL, PWD,"0","2020-10-15");

                //Affichage en console du body de la réponse du POST
                System.out.println(creation.prettyPrint());

                System.out.println("assertions sur le body de retour- code- firstname - lastname et et mail");
                creation.then().assertThat().statusCode(201) // assertion sur le code de retour
                        .body("prestashop.customer.lastname", equalTo(LASTNAME)) // assertion sur le lastname
                        .body("prestashop.customer.firstname", equalTo(FIRSTNAME)) //assertion sur le firstname
                        .body("prestashop.customer.email", equalTo(EMAIL)); // assertion sur l'email


                //password généré automatiquement de 60 caractères
                System.out.println("Assert sur le password crypté: il est bien de 60 caractères random");
                creation.then().assertThat().body("prestashop.customer.passwd", matchesPattern(".{60}"));


                System.out.println("recupération de l'id");
                 ID = creation.path("prestashop.customer.id").toString();

                //affiche l'id
                System.out.println("l'id du customer est : "+ID);

                System.out.println("***pas 3- Rechercher le client créé et vérifier que le compte n'est pas actif");
                Response customer = api.getCustomerWithId(ID);

                //affichage du body en reponse
                System.out.println("retour du GET sur le customer :"+customer.prettyPrint());

                System.out.println("Assert sur le body de retour");
                customer.then().assertThat()
                        .statusCode(200) // sur le code de retour
                        .body("prestashop.customer.id", equalTo(ID))// assert sur l'id
                        .body("prestashop.customer.lastname", equalTo(LASTNAME)) // assertion sur le lastname
                        .body("prestashop.customer.firstname", equalTo(FIRSTNAME)) //assertion sur le firstname
                        .body("prestashop.customer.email", equalTo(EMAIL)); // assertion sur l'email

                System.out.println("Assert sur le noeud Active qui est à 0");
                customer.then().assertThat().body("prestashop.customer.active", equalTo("0"));

                System.out.println("***pas 4 -Changer la valeur 'active' à 1 avec la méthode PATCH");
                // méthode d'update avec un patch
                Response statut= api.updateCustomerStatus(ID,"1");
                System.out.println("réponse au put" +statut.prettyPrint());

                System.out.println("Assertion sur le retour du body (code retour, id, nom, prenom, email");
                statut.then().assertThat()
                        .statusCode(200)
                        .body("prestashop.customer.id", equalTo(ID))// assert sur l'id
                        .body("prestashop.customer.lastname", equalTo(LASTNAME)) // assertion sur le lastname
                        .body("prestashop.customer.firstname", equalTo(FIRSTNAME)) //assertion sur le firstname
                        .body("prestashop.customer.email", equalTo(EMAIL)); // assertion sur l'email

                System.out.println("Assert sur le noeud Active à 1");
                statut.then().assertThat().body("prestashop.customer.active", equalTo("1"));

                System.out.println("**** pas 5- se connecter à l'application");
                System.out.println("se connecter à l'url de prestashop");
                driver.get(URL);

                System.out.println("Assert Arrivée sur la page d'accueil-titre de la page");
                assertEquals(driver.getTitle(),"prestashop");

                System.out.println("Cliquer sur le boutton Se connecter");
                //instanciation page accueil
                WelcomePage welcomePage = new WelcomePage(driver);

                //click boutton "Se connecter" depuis la page d'accueil"
                ConnectionPage connectionPage = welcomePage.clickConnexion(wait);

                System.out.println("Remplir le formulaire de connexion");
                System.out.println("Remplir l'identifiant");
                connectionPage.fillUsername(wait, EMAIL);

                System.out.println("Remplir le mot de passe");
                connectionPage.fillPassword(wait, PWD);

                System.out.println("cliquer sur le bouton Se connecter");
                connectionPage.clickConnexion(wait);

                System.out.println("La page du compte s'affiche");
                //instanciation du header
                HeadBand headBand= new HeadBand(driver);
                //vérification avec le nom du client qui s'affiche en haut à droite
                String nomComplet = FIRSTNAME +" "+ LASTNAME;
                System.out.println("nom complet " +nomComplet);
                assertEquals(nomComplet,headBand.getNomClient(wait).getText());

                System.out.println("***pas 6 - se déconnecter***");
                System.out.println("cliquer sur se déconnecter");
                headBand.getDeconnexion(wait).click();

                System.out.println("Assert déconnexion avec la présence de connexion");
                //par le texte
                assertEquals(headBand.connexionIconText(),"Connexion");
                //par la présence
                assertTrue(headBand.getConnexionTxt(wait).isDisplayed());

                System.out.println("***pas 7- supprimer le client créé ");
                Response delete= api.deleteCustomer(ID);
                delete.then().assertThat().statusCode(200);

                System.out.println("***pas 8- rechercher les clients disponibles");
                Response allCustomers = api.getAllCustomers();
                //assert sur le code de retour
                allCustomers.then().statusCode(200);

                //affichage en console
                System.out.println(allCustomers.prettyPrint());

                System.out.println("Assert que l'id "+ID+" n'est pas présent");
                String allCustomerString =allCustomers.asString();
                assertThat(allCustomerString, not(containsString("customer id=\"" + ID + "\"")));

                System.out.println("***pas 9- Se connecter avec le client supprimé");
                System.out.println("cliquer sur se connecter en haut à droite'");
                connectionPage= welcomePage.clickConnexion(wait);

                System.out.println("Remplissage du username");
                connectionPage.fillUsername(wait, EMAIL);

                System.out.println("Remplissage du pwd");
                connectionPage.fillPassword(wait, PWD);

                System.out.println("Cliquer sur le bouton Se connecter");
                connectionPage.clickConnexion(wait);

                System.out.println("Assert sur la bannière Erreur d'authentification");
                assertTrue(connectionPage.getAuthFail().isDisplayed(),"La bannière indiquant l'erreur d'authentification n'est pas présente");
                assertEquals("Échec d'authentification",connectionPage.getAuthFail().getText(),"on attend un message d'erreur d'authentification");


        }


}
