package org.prestashop.api;

import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.prestashop.utils.ApiMethodes;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecuperationProduitSansToken {

    @Test
    public void testRecuperationProduitSansToken(){
        System.out.println("***Pas 1 - Récupérer les produits sans token");
        //initialisation des méthodes API
        ApiMethodes api= new ApiMethodes();

        System.out.println("***faire le GET sans authorisation");
        Response noProduct= api.getProductwithoutKey();

        System.out.println("reponse de la requete GET : "+ noProduct.prettyPrint());

        System.out.println("Assert sur le code retour 401");
        noProduct.then().assertThat().statusCode(401);

        System.out.println("Assert sur le message de retour");
        String errorMsg= noProduct.body().asString();
        System.out.println("Message d'erreur : "+ errorMsg);
        assertEquals("401 Unauthorized", errorMsg,"Le message d'erreur n'est pas le bon");




    }
}
