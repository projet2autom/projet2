package org.prestashop.api;

import com.beust.ah.A;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.prestashop.utils.ApiMethodes;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecuperationProduitIncorrectToken {

    @Test
    public void testRecuperationIncorrectToken(){
        System.out.println("***pas 1 - Rechercher la liste des produits avec une clé sans droit sur la catégorie produit");
        System.out.println(" faire le get");
        //instanciation de la classe Méthode Api
        ApiMethodes api= new ApiMethodes();

        //GET/products
        Response getProducts = api.getProductNoRightsKey();
        System.out.println("message de retour de la requête GET" +getProducts.prettyPrint());

        System.out.println("Assert sur le code retour");
        getProducts.then().assertThat().statusCode(401);

        System.out.println("Assert sur le message de retour");
        String msgError= getProducts.path("prestashop.errors.error.message").toString();
        System.out.println("Contenu du message d'erreur : "+ msgError);
        assertEquals("Resource of type \"products\" is not allowed with this authentication key", msgError,"Le message d'erreur n'est pas le bon");




    }



}
