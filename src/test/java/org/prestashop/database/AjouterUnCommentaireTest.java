package org.prestashop.database;

import org.junit.jupiter.api.Test;
import org.prestashop.selenium.*;
import org.prestashop.utils.DBTools;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AjouterUnCommentaireTest extends AbstractTest {

    private final String querySelect = "SELECT * from ps_product_comment ppc";
    private final String queryUpdate = "UPDATE ps_product_comment SET validate  = '1' WHERE id_product_comment = '%s'";
    private final String queryDelete = "DELETE from ps_product_comment WHERE id_product_comment = '%s'";
    private final String urlPrestashop = "http://192.168.102.174:8081/";
    private final String username = "pub@prestashop.com";
    private final String password = "prestashop";

    private final String product = "Pull imprimé colibri";
    private final String titre = "titre du commentaire";
    private final String commentaire = "Ceci est mon commentaire pour ce produit";
    private final String grade = "4";


    @Test
    public void testAjouterUnCommentaire() throws Throwable {

        logger.info("ouverture de prestashop");
        driver.get(urlPrestashop);
        logger.info("on est sur la page");

        WelcomePage welcomePage = new WelcomePage(driver);
        HeadBand headBand = new HeadBand(driver);
        logger.info("connection à prestashop");
        ConnectionPage connectionPage = headBand.clickbtnConnexion(wait);
        connectionPage.fillUsername(wait, username);
        connectionPage.fillPassword(wait, password);
        AccountPage accountPage = connectionPage.clickConnexion(wait);
        accountPage.goToHomePage();

        logger.info("clique sur le produit");
        ProductPage productPage = welcomePage.clickOnProduct(product);
        logger.info("on est sur la page du produit");

        logger.info("clique sur 'Soyez le premier à donner votre avis'");
        productPage.premierCommentaireClick(wait);
        logger.info("visibility of popup");

        logger.info("renseigner l'avis");
        productPage.commentaire(wait, titre, commentaire);
        productPage.selectStar(wait, grade);
        productPage.envoyerCommentaire(wait);
        logger.info("vérifier avis ok");
        assertTrue(productPage.avisPoster(wait));

        logger.info("fermeture de la popup");
        productPage.fermerAvis(wait);
        logger.info("vérification nombre de commentaire");
        assertEquals("0", productPage.numberOfComment());

        DBTools dbTools = new DBTools();
        dbTools.getConnection();

        Map<String, String> result = dbTools.query(querySelect);
        assertEquals(titre,result.get("title"));
        assertEquals(commentaire,result.get("content"));
        assertEquals(grade,result.get("grade").substring(0,1),"la note affiché n'est pas bonne");
        String idComment = result.get("id_product_comment");

        logger.info("update validation du commentaire ");
        String updateQ = String.format(queryUpdate,idComment);
        System.out.println("update : "+updateQ);
        dbTools.updateDB(updateQ);
        logger.info("vérification de l'update");
        result = dbTools.query(querySelect);
        assertEquals("1",result.get("validate"));

        logger.info("refresh de la page");
        driver.navigate().refresh();
        logger.info("vérification du commentaire");
        assertEquals(titre,productPage.getCommentTitle(wait));
        assertEquals(commentaire,productPage.getCommentContent(wait));
        assertEquals(grade,productPage.getCommentGrade());

        logger.info("suppression du commentaire");
        String deleteQ = String.format(queryDelete,idComment);
        System.out.println("update : "+deleteQ);
        dbTools.updateDB(deleteQ);


        logger.info("refresh page");
        driver.navigate().refresh();
        logger.info("commentaire visible ?");
        assertFalse(productPage.commentVisible(wait));





    }
}
