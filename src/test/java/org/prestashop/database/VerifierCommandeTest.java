package org.prestashop.database;

import org.junit.jupiter.api.Test;
import org.prestashop.selenium.*;
import org.prestashop.utils.DBTools;

import java.util.Map;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

public class VerifierCommandeTest extends AbstractTest {

    private final String querySelect = "select pso.id_order ,psc.firstname, psc.lastname, pso.reference , pso.total_paid , pso.payment , pso.module, pod.product_name , pod.product_quantity , pod.total_price_tax_incl , pa.address1 , pa.city\n" +
            "from ps_orders pso join ps_customer psc\n" +
            "join ps_order_detail pod \n" +
            "join ps_product pp \n" +
            "join ps_address pa \n" +
            "where pso.id_customer = psc.id_customer \n" +
            "and pso.id_customer = pa.id_customer \n" +
            "and pso.id_order = pod.id_order\n" +
            "and pod.product_id = pp.id_product \n" +
            "and pa.id_country = '8'\n" +
            "and pso.reference = '%s'";
    private final String urlPrestashop = "http://192.168.102.174:8081/";
    private final String username = "pub@prestashop.com";
    private final String password = "prestashop";

    private final String product = "Pull imprimé colibri";
    private final String nom = "doe";
    private final String prenom = "john";
    private final String adress = "16, Main street Paris ";
    private final String total = "34.46";
    private final String typePaiement = "Transfert bancaire";
    private final String prixProduit = "34.46";
    private final String quantiteProduit = "1";



    @Test
    public void testVerifierCommande() throws Throwable {

        logger.info("ouverture de prestashop");
        driver.get(urlPrestashop);
        logger.info("on est sur la page");

        WelcomePage welcomePage = new WelcomePage(driver);
        HeadBand headBand = new HeadBand(driver);
        logger.info("connection à prestashop");
        ConnectionPage connectionPage = headBand.clickbtnConnexion(wait);
        connectionPage.fillUsername(wait, username);
        connectionPage.fillPassword(wait, password);
        AccountPage accountPage = connectionPage.clickConnexion(wait);
        accountPage.goToHomePage();

        logger.info("clique sur le produit");
        ProductPage productPage = welcomePage.clickOnProduct(product);
        logger.info("on est sur la page du produit");

        logger.info("ajouter le produit au panier");
        String price = "";
        PopUpRecapPage popUpRecapPage = productPage.addProductToCart(wait);
        logger.info("popup 1 article, transport gratuit et prix +ttc ok");

        logger.info("clique sur commander");
        CartPage cartPage = popUpRecapPage.clickCommander(wait);
        logger.info("");

        logger.info("valider panier");
        OrderPage orderPage = cartPage.clickCommander(wait);
        logger.info("info perso affiché");
//        orderPage.getPageInfoTitle(wait);

        logger.info("selection adresse française,retrait, payement et validation du panier");
        orderPage.clickContinuer1(wait);

        logger.info("continuer vers mode de paiement");
        orderPage.clickContinuer2(wait);

        logger.info("accepter cgu");
        orderPage.clickConditionsGenerales(wait);

        logger.info("selectionner paiement");
        orderPage.clickPayByCard(wait);

        logger.info("commander");
        RecapCommandPage recapCommandPage = orderPage.clickCommander(wait);

        logger.info("récupération de la reference commande");
        String commandRef = recapCommandPage.getReference(wait);

        String query = String.format(querySelect,commandRef);

        DBTools dbTools = new DBTools();
        dbTools.getConnection();
        Map<String,String> result = dbTools.query(query);
        assertEquals(nom.toLowerCase(),result.get("lastname").toLowerCase());
        assertEquals(prenom.toLowerCase(),result.get("firstname").toLowerCase());
        String adressClient = String.format("%s %s",result.get("address1"),result.get("city"));
        assertEquals(adress.toLowerCase(),adressClient.toLowerCase());
        assertEquals(total,result.get("total_paid").substring(0,5));
        assertEquals(typePaiement.toLowerCase(),result.get("payment").toLowerCase());
        assertTrue(result.get("product_name").toLowerCase().contains(product.toLowerCase()));
        assertEquals(prixProduit,result.get("total_price_tax_incl").substring(0,5));
        assertEquals(quantiteProduit,result.get("product_quantity"));


//        System.out.println("clean bdd → order id : "+result.get("id_order"));
//        String queryDelCommand = String.format("DELETE from ps_orders WHERE id_order = '%s'",result.get("id_order"));
//        String queryDelCommandDetail = String.format("DELETE from ps_order_detail WHERE id_order = '%s'",result.get("id_order"));
//
//        dbTools.updateDB(queryDelCommandDetail);
//        dbTools.updateDB(queryDelCommand);
    }
}
